# Video Processor

## Description

This is a simple script that takes a folder containing videos as an input and gives the user an option to apply some processing steps on all the videos in that folder. The processing steps available right now are rotation by 90deg and converting to grayscale.

## Installation

This script is easy to install on any system and start processing videos. Follow these easy steps for installation:

1. Clone this repository

2. Install all the requirements

    `pip install -r requirements.txt`

3. Use the script to start processing bulk videos!

    Find the usage details in the next section.

## Usage

Run the python script with a folder path input as shown:

`python app.py -i path/to/folder/containing/videos`

usage: app.py [-h] -i [--rotate] [--no-rotate] [--grayscale] [--no-grayscale]

optional arguments:

-h, --help - Show this help message and exit

-i , --input - Input Folder Path (Required)

--rotate - Default True

--no-rotate

--grayscale - Default True

--no-grayscale

## Support

It is all opensource, so contact me if you...

1. ...have some valuable inputs to share
2. ...want to work on it
3. ...find any bug
4. ...want to share a feedback on the work
5. ...etc.

Send me a mail at [tejasmorkar@gmail.com](tejasmorkar@gmail.com) or [create a new Issue](https://gitlab.com/tejasmorkar/video-processor/-/issues/new) on this repository.
You can also contact me through my [LinkedIn Profile](https://www.linkedin.com/in/tejasmorkar/).

## Roadmap

This script can be further advanced as per below mentioned points and more:

-   Adding rotation angles for video frames
-   Options for other filters than grayscale

## License

This project is freely available for free non-commercial/ commercial use, and may be redistributed under these conditions. Please, see the [license](./LICENSE) for further details.
