import os
import sys
import argparse
import numpy as np
import cv2

### Argument Parser Start

desc = "This is a script used to process a video to rotate its frames by 90deg and convert into grayscale. Finally, you can also save it as a new video."
parser = argparse.ArgumentParser(description = desc)
parser.add_argument("-i", "--input", metavar='', type=str, required=True, help="Input Folder Path")

parser.add_argument('--rotate', dest='rotate', action='store_true')
parser.add_argument('--no-rotate', dest='rotate', action='store_false')
parser.set_defaults(rotate=True)

parser.add_argument('--grayscale', dest='grayscale', action='store_true')
parser.add_argument('--no-grayscale', dest='grayscale', action='store_false')
parser.set_defaults(grayscale=True)

args = parser.parse_args()

### Argument Parser End

### Function definitions Start

def rotateFrame(frame, angle):
    w, h = frame.shape[1::-1]
    (cX, cY) = (w // 2, h // 2)
    M = cv2.getRotationMatrix2D((cX, cY), angle, 1)
    cos = np.abs(M[0, 0])
    sin = np.abs(M[0, 1])
    nW = int((h * sin) + (w * cos))
    nH = int((h * cos) + (w * sin))
    M[0, 2] += (nW / 2) - cX
    M[1, 2] += (nH / 2) - cY
    return cv2.warpAffine(frame, M, (nW, nH))

def startProgress(title):
    global progress_x
    sys.stdout.write(title + ": [" + "-"*40 + "]" + chr(8)*41)
    sys.stdout.flush()
    progress_x = 0

def progress(x):
    global progress_x
    x = int(x * 40 // 100)
    sys.stdout.write("#" * (x - progress_x))
    sys.stdout.flush()
    progress_x = x

def endProgress(file):
    sys.stdout.write("#" * (40 - progress_x) + "]\n")
    sys.stdout.flush()
    print("Video saved as Output - {}.avi".format(file[:-4]))

### Function definitions End

total_video_files = sum(1 for i in os.listdir(args.input) if i.endswith(".mp4") or i.endswith(".avi"))

if total_video_files == 0:
    print("No Video Files Found in specified Input Folder")
else:
    print("Found {} video file/s.".format(total_video_files))

file_count = 0

# Loop over all the files in the folder
for file in os.listdir(args.input):
    if file.endswith(".mp4") or file.endswith(".avi"):
        file_count += 1
        print("\n>> Starting file {} of {}".format(file_count, total_video_files))
        path=os.path.join(args.input, file)

        cap = cv2.VideoCapture(path)
        fps = cap.get(cv2.CAP_PROP_FPS)
        width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
        height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)
        n_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

        print("Video Details:")
        print("FPS: {}".format(fps))

        if args.rotate == False:
            # Swap width and height values
            width = height + width
            height = width - height
            width = width - height

        fourcc = cv2.VideoWriter_fourcc(*'DIVX')
        out = cv2.VideoWriter("Output - {}.avi".format(file[:-4]), fourcc, fps, (int(height), int(width)), isColor=not args.grayscale)
            
        progress_int = 0
        startProgress("Video Processing")

        while(True):
            ret, frame = cap.read()

            if ret:
                if args.grayscale:
                    frame = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)

                if args.rotate:
                    frame = rotateFrame(frame, 90)

                out.write(frame)
                progress_int += 1
                progress((progress_int/n_frames)*100)

                if cv2.waitKey(20) & 0xFF == ord('q'):
                    break
            else:
                break

        endProgress(file)
        out.release()
        cap.release()
        cv2.destroyAllWindows()